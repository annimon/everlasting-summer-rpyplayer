package com.annimon.everlastingsummer;

import java.util.Locale;
import android.text.TextUtils;

/**
 * Корректировщик путей к ресурсам.
 * @author aNNiMON
 */
public final class PathResolver {
    
    public static final String SCRIPT_ASSETS = "scripts";
    private static final String SPRITE = "sprites/";
    private static final String MUSIC = "music/";
    private static final String SOUND = "sfx/";
    
    private static final String PNG = ".png";
    private static final String JPG = ".jpg";
    private static final String OGG = ".ogg";
    
    public static String script(String name) {
        return SCRIPT_ASSETS + "/" + name;
    }
    
    public static String sprite(String whoid, String params) {
        final StringBuilder sb = new StringBuilder();
        sb.append(SPRITE);
        sb.append(whoid.toLowerCase(Locale.ENGLISH)).append('/');
        sb.append(TextUtils.isEmpty(params) ? "normal" : params);
        sb.append(PNG);
        return sb.toString();
    }
    
    public static String background(String type, String name) {
        final StringBuilder sb = new StringBuilder();
        sb.append(type.toLowerCase(Locale.ENGLISH)).append('/');
        sb.append(name);
        sb.append(JPG);
        return sb.toString();
    }

    public static String music(String name) {
        final StringBuilder sb = new StringBuilder();
        sb.append(MUSIC);
        sb.append(name);
        sb.append(OGG);
        return sb.toString();
    }
    
    public static String sound(String name) {
        final StringBuilder sb = new StringBuilder();
        sb.append(SOUND);
        if (name.startsWith("sfx_")) sb.append(name.substring(4));
        else sb.append(name);
        sb.append(OGG);
        return sb.toString();
    }
    
}
