package com.annimon.everlastingsummer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author aNNiMON
 */
public final class Lexer {
    
    public static List<Token> tokenize(String input) {
        return new Lexer(input).process().getTokens();
    }
    
    private static final String OPERATOR_CHARS = "=+-<>()[]!$:";
    private static final TokenType[] OPERATOR_TYPES = new TokenType[] {
        TokenType.EQ,
        TokenType.PLUS, TokenType.MINUS,
        TokenType.LT, TokenType.GT,
        TokenType.LPAREN, TokenType.RPAREN, TokenType.LBRACKET, TokenType.RBRACKET,
        TokenType.EXCL, TokenType.COMMAND, TokenType.COLON,
    };

    private static final Map<String, TokenType> KEYWORDS;
    static {
        KEYWORDS = new HashMap<String, TokenType>();
        KEYWORDS.put("play", TokenType.PLAY);
        KEYWORDS.put("queue", TokenType.QUEUE);
        KEYWORDS.put("stop", TokenType.STOP);
        KEYWORDS.put("music", TokenType.MUSIC);
        KEYWORDS.put("ambience", TokenType.AMBIENCE);
        KEYWORDS.put("sound", TokenType.SOUND);
        KEYWORDS.put("sound_loop", TokenType.SOUNDLOOP);
        KEYWORDS.put("fadein", TokenType.FADEIN);
        KEYWORDS.put("fadeout", TokenType.FADEOUT);
        
        KEYWORDS.put("scene", TokenType.SCENE);
        KEYWORDS.put("anim", TokenType.ANIM);
        KEYWORDS.put("bg", TokenType.BG);
        KEYWORDS.put("cg", TokenType.CG);
        KEYWORDS.put("at", TokenType.AT);
        KEYWORDS.put("as", TokenType.AS);
        KEYWORDS.put("define", TokenType.DEFINE);
        KEYWORDS.put("window", TokenType.WINDOW);
        KEYWORDS.put("hide", TokenType.HIDE);
        KEYWORDS.put("show", TokenType.SHOW);
        KEYWORDS.put("with", TokenType.WITH);
        
        KEYWORDS.put("return", TokenType.RETURN);
        KEYWORDS.put("menu", TokenType.MENU);
        KEYWORDS.put("endmenu", TokenType.ENDMENU);
        KEYWORDS.put("jump", TokenType.JUMP);
        KEYWORDS.put("label", TokenType.LABEL);
        
        KEYWORDS.put("if", TokenType.IF);
        KEYWORDS.put("else", TokenType.ELSE);
        KEYWORDS.put("endif", TokenType.ENDIF);
        KEYWORDS.put("or", TokenType.OR);
        KEYWORDS.put("and", TokenType.AND);
        KEYWORDS.put("not", TokenType.NOT);
        
        KEYWORDS.put("renpy.pause", TokenType.RENPY_PAUSE);
        KEYWORDS.put("renpy.say", TokenType.RENPY_SAY);
        KEYWORDS.put("persistent.sprite_time", TokenType.PERSISTENT_SPRITE_TIME);
        KEYWORDS.put("prolog_time", TokenType.PROLOG_TIME);
        KEYWORDS.put("day_time", TokenType.DAY_TIME);
        KEYWORDS.put("sunset_time", TokenType.SUNSET_TIME);
        KEYWORDS.put("night_time", TokenType.NIGHT_TIME);
        KEYWORDS.put("make_names_known", TokenType.MAKE_NAMES_KNOWN);
        KEYWORDS.put("make_names_unknown", TokenType.MAKE_NAMES_UNKNOWN);
        KEYWORDS.put("set_name", TokenType.SET_NAME);
        KEYWORDS.put("meet", TokenType.SET_NAME);
        KEYWORDS.put("disable_all_zones", TokenType.DISABLE_ALL_ZONES);
        KEYWORDS.put("disable_current_zone", TokenType.DISABLE_CURRENT_ZONE);
        KEYWORDS.put("reset_zone", TokenType.RESET_ZONE);
        KEYWORDS.put("set_zone", TokenType.SET_ZONE);
        KEYWORDS.put("show_map", TokenType.SHOW_MAP);
    }
    
    private final List<Token> tokens;
    private final StringBuilder buffer;
    
    private final String input;
    private final int length;
    
    private int pos;
    
    private Lexer(String input) {
        this.input = input;
        this.length = input.length();
        
        tokens = new ArrayList<Token>();
        buffer = new StringBuilder();
    }
    
    public List<Token> getTokens() {
        return tokens;
    }
    
    public Lexer process() {
        pos = 0;
        while (pos < length) {
            tokenize();
        }
        addToken(TokenType.EOF);
        return this;
    }
    
    private void tokenize() {
        skipWhitespaces();
        final char ch = peek(0);
        if (Character.isLetter(ch)) {
            // Слово (ключевое слово или команда)
            tokenizeWord();
        } else if (Character.isDigit(ch)) {
            // Число
            tokenizeNumber();
        } else if (ch == '"' || ch == '\'') {
            // Текст в "кавычках" или 'одинарных'
            tokenizeText(ch);
        } else if (ch == '#') {
            tokenizeComment();
        } else {
            // Операторы и спецсимволы
            tokenizeOperator();
        }
    }
    
    private void tokenizeWord() {
        char ch = peek(0);
        // Строка в юникоде u"текст" или u'текст'
        if (ch == 'u') {
            final char textStartChar = peek(1);
            if (textStartChar == '"' || textStartChar == '\'') {
                next(); // u
                tokenizeText(textStartChar);
                return;
            }
        }
        
        clearBuffer();
        while(Character.isLetterOrDigit(ch) || (ch == '_') || (ch == '.')) {
            buffer.append(ch);
            ch = next();
        }
        
        final String word = buffer.toString();
        final String key = word.toLowerCase(Locale.ENGLISH);
        if (KEYWORDS.containsKey(key)) {
            addToken(KEYWORDS.get(key));
        } else {
            addToken(TokenType.WORD, word);
        }
    }
    
    private void tokenizeNumber() {
        char ch = peek(0);
        clearBuffer();
        boolean decimal = false;
        while (true) {
            // Целое или вещественное число.
            if (ch == '.') {
                // Пропускаем десятичные точки, если они уже были в числе.
                if (!decimal) buffer.append(ch);
                decimal = true;
                ch = next();
                continue;
            } else if (!Character.isDigit(ch)) {
                break;
            }
            buffer.append(ch);
            ch = next();
        }
        addToken(TokenType.NUMBER, buffer.toString());
    }
    
    private void tokenizeOperator() {
        final char ch = peek(0);
        final int index = OPERATOR_CHARS.indexOf(ch);
        if (index != -1) {
            addToken(OPERATOR_TYPES[index]);
        }
        next();
    }
    
    private void tokenizeText(final char textStartChar) {
        clearBuffer();
        char ch = next(); // пропускаем открывающую кавычку
        while(true) {
            if (ch == textStartChar) break;
            if (ch == '\0') break; // не закрыта кавычка, но мы добавим то, что есть
            if (ch == '\\') {
                ch = next();
                switch (ch) {
                    case 'n': ch = next(); buffer.append('\n'); continue;
                    case 't': ch = next(); buffer.append('\t'); continue;
                    default:
                        if (ch == textStartChar) {
                            ch = next();
                            buffer.append('"');
                            continue;
                        }
                }
                buffer.append('\\');
                continue;
            }
            buffer.append(ch);
            ch = next();
        }
        next(); // пропускаем закрывающую кавычку
        addToken(TokenType.TEXT, buffer.toString());
    }
    
    private void tokenizeComment() {
        char ch = peek(0);
        while("\n\r\0".indexOf(ch) == -1) {
            ch = next();
        }
    }
    
    private void skipWhitespaces() {
        char ch = peek(0);
        while(ch != '\0' && Character.isWhitespace(ch)) {
            ch = next();
        }
    }
    
    private void addToken(TokenType type) {
        addToken(type, "");
    }
    
    private void addToken(TokenType type, String text) {
        tokens.add(new Token(text, type));
    }

    private void clearBuffer() {
        buffer.setLength(0);
    }
    
    private char next() {
        pos++;
        if (pos >= length) return '\0';
        return input.charAt(pos);
    }
    
    private char peek(int relativePosition) {
        int tempPos = pos + relativePosition;
        if (tempPos >= length) return '\0';
        return input.charAt(tempPos);
    }
}
