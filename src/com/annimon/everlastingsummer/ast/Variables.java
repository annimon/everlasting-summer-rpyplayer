package com.annimon.everlastingsummer.ast;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс для работы с переменными.
 * @author aNNiMON
 */
public class Variables {

    private static Map<String, Double> variables;
    
    public static void init() {
        if (variables == null) {
            variables = new HashMap<String, Double>();
        } else {
            variables.clear();
        }
        variables.put("True", 1d);
        variables.put("False", 0d);
    }
    
    public static Map<String, Double> getVariables() {
        return variables;
    }
    
    public static void setVariables(Map<String, Double> variables) {
        Variables.variables.putAll(variables);
    }

    public static double getVariable(String var) {
        if (!variables.containsKey(var)) return 0;
        return variables.get(var);
    }
    
    public static void setVariable(String var, double value) {
        variables.put(var, value);
    }
}
