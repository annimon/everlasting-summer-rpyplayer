package com.annimon.everlastingsummer.ast;

/**
 * @author aNNiMON
 */
public class BinaryExpression implements Expression {
    
    public static enum Operator {
        ADD, SUBTRACT,
        BOOLEAN_OR, BOOLEAN_AND,
        EQUALS, NOTEQUALS,
        GT, GTEQ, LT, LTEQ
    }

    public final Operator operator;
    private final Expression left, right;

    public BinaryExpression(Operator op, Expression left, Expression right) {
        this.operator = op;
        this.left = left;
        this.right = right;
    }

    @Override
    public double eval() {
        final double leftVal = left.eval();
        final double rightVal = right.eval();
        switch (operator) {
            case ADD: return leftVal + rightVal;
            case SUBTRACT: return leftVal - rightVal;
            case EQUALS: return (leftVal == rightVal) ? 1 : 0;
            case NOTEQUALS: return (leftVal != rightVal) ? 1 : 0;
            case BOOLEAN_OR: return ((leftVal != 0) || (rightVal != 0)) ? 1 : 0;
            case BOOLEAN_AND: return ((leftVal != 0) && (rightVal != 0)) ? 1 : 0;
            case GT: return (leftVal > rightVal) ? 1 : 0;
            case LT: return (leftVal < rightVal) ? 1 : 0;
            case GTEQ: return (leftVal >= rightVal) ? 1 : 0;
            case LTEQ: return (leftVal <= rightVal) ? 1 : 0;
            default:
                throw new RuntimeException("Неизвестный оператор " + operator.name());
        }
    }
}
