package com.annimon.everlastingsummer;

/**
 * Персонажи бесконечного лета.
 * @author aNNiMON
 */
public final class CharactersES extends Characters {
    
    @Override
    public void makeNamesUnknown() {
        super.makeNamesUnknown();
        // https://github.com/yakui-lover/eroge-dopil/blob/master/media.rpy#L365
        names.put("me", new NameInfo("Семён", 0xFFE1DD7D));
        names.put("un", new NameInfo("Пионерка", 0xFFB956FF));
        names.put("dv", new NameInfo("Пионерка", 0xFFFFAA00));
        names.put("sl", new NameInfo("Пионерка", 0xFFFFD200));
        names.put("us", new NameInfo("Пионерка", 0xFFFF3200));
        names.put("mt", new NameInfo("Вожатая", 0xFF00EA32));
        names.put("cs", new NameInfo("Медсестра", 0xFFA5A5FF));
        names.put("mz", new NameInfo("Пионерка", 0xFF4A86FF));
        names.put("mi", new NameInfo("Пионерка", 0xFF00DEFF));
        names.put("uv", new NameInfo("Странная девочка", 0xFF4EFF00));
        names.put("lk", new NameInfo("Луркмор-кун", 0xFFFF8080));
        names.put("sh", new NameInfo("Пионер", 0xFFFFF226));
        names.put("el", new NameInfo("Пионер", 0xFFFFFF00));
        names.put("pi", new NameInfo("Пионер", 0xFFE60101));
        
        names.put("dy", new NameInfo("Голос из динамика", 0xFFC0C0C0));
        names.put("voice", new NameInfo("Голос", 0xFFE1DD7D));
        names.put("voices", new NameInfo("Голоса", 0xFFC0C0C0));
        names.put("message", new NameInfo("Сообщение", 0xFFC0C0C0));
        names.put("all", new NameInfo("Пионеры", 0xFFED4444));
        names.put("kids", new NameInfo("Малышня", 0xFFEB7883));
        names.put("dreamgirl", new NameInfo("...", 0xFFC0C0C0));
        names.put("bush", new NameInfo("Голос", 0xFFC0C0C0));
        names.put("FIXME_voice", new NameInfo("Голос", 0xFFC0C0C0));
        names.put("odn", new NameInfo("Одногруппник", 0xFFC0C0C0));
        names.put("mt_voice", new NameInfo("Голос", 0xFF00EA32));
    }

    @Override
    public void makeNamesKnown() {
        super.makeNamesKnown();
        setName("me", "Семён");
        setName("un", "Лена");
        setName("dv", "Алиса");
        setName("sl", "Славя");
        setName("us", "Ульяна");
        setName("mt", "Ольга Дмитриевна");
        setName("cs", "Виола");
        setName("mz", "Женя");
        setName("mi", "Мику");
        setName("uv", "Юля");
        setName("lk", "Луркмор-кун");
        setName("sh", "Шурик");
        setName("el", "Электроник");
        setName("pi", "Пионер");
        setName("dy", "Голос из динамика");
        setName("voice", "Голос");
        setName("voices", "Голоса");
        setName("message", "Сообщение");
        setName("all", "Пионеры");
        setName("kids", "Малышня");
        setName("dreamgirl", "...");
        setName("bush", "Голос");
        setName("FIXME_voice", "Голос");
        setName("odn", "Одногруппник");
        setName("mt_voice", "Голос");
    }
}
