package com.annimon.everlastingsummer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Информация о сохранении.
 * @author aNNiMON
 */
public final class SaveInfo implements Parcelable {

    private String path;
    private long time;
    private int position;
    private Map<String, Double> variables;
    private String backgroundType, backgroundName;
    
    public SaveInfo() {}
    
    public SaveInfo(String path, long time, int position, Map<String, Double> variables,
            String bgType, String bgName) {
        this.path = path;
        this.time = time;
        this.position = position;
        this.variables = variables;
        this.backgroundType = bgType;
        this.backgroundName = bgName;
    }
    
    private SaveInfo(Parcel in) {
        path = in.readString();
        time = in.readLong();
        position = in.readInt();
        final int varSize = in.readInt();
        variables = new HashMap<String, Double>(varSize);
        for (int i = 0; i < varSize; i++) {
            variables.put(in.readString(), in.readDouble());
        }
        try {
            backgroundType = in.readString();
            backgroundName = in.readString();
        } catch (Exception ioe) {}
    }
    
    private SaveInfo(DataInputStream in) throws IOException {
        path = in.readUTF();
        time = in.readLong();
        position = in.readInt();
        final int varSize = in.readInt();
        variables = new HashMap<String, Double>(varSize);
        for (int i = 0; i < varSize; i++) {
            variables.put(in.readUTF(), in.readDouble());
        }
        try {
            backgroundType = in.readUTF();
            backgroundName = in.readUTF();
        } catch (Exception ioe) {}
    }

    public String getPath() {
        return path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }
    
    public long getTime() {
        return time;
    }
    
    public void setTime(long time) {
        this.time = time;
    }
    
    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Map<String, Double> getVariables() {
        return variables;
    }
    
    public void setVariables(Map<String, Double> variables) {
        this.variables = variables;
    }
    
    public String getBackgroundType() {
        return backgroundType;
    }
    
    public void setBackgroundType(String backgroundType) {
        this.backgroundType = backgroundType;
    }
    
    public String getBackgroundName() {
        return backgroundName;
    }
    
    public void setBackgroundName(String backgroundName) {
        this.backgroundName = backgroundName;
    }

    public static SaveInfo readFromStream(DataInputStream dis) throws IOException {
        return new SaveInfo(dis);
    }
    
    public void writeToStream(DataOutputStream dos) throws IOException {
        dos.writeUTF(path);
        dos.writeLong(time);
        dos.writeInt(position);
        dos.writeInt(variables.size());
        for (Map.Entry<String, Double> entry : variables.entrySet()) {
            dos.writeUTF(entry.getKey());
            dos.writeDouble(entry.getValue());
        }
        dos.writeUTF(backgroundType);
        dos.writeUTF(backgroundName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeLong(time);
        dest.writeInt(position);
        dest.writeInt(variables.size());
        for (Map.Entry<String, Double> entry : variables.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeDouble(entry.getValue());
        }
        dest.writeString(backgroundType);
        dest.writeString(backgroundName);
    }
    
    public static final Parcelable.Creator<SaveInfo> CREATOR = new Parcelable.Creator<SaveInfo>() {
        @Override
        public SaveInfo createFromParcel(Parcel in) {
            return new SaveInfo(in);
        }

        @Override
        public SaveInfo[] newArray(int size) {
            return new SaveInfo[size];
        }
    };
}
