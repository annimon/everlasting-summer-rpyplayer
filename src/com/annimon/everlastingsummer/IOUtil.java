package com.annimon.everlastingsummer;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

/**
 * Класс для работы с файловой системой.
 * @author aNNiMON
 */
public final class IOUtil {
    
    public static boolean useArchive = false;
    public static String ASSETS = getSdCardPath() + "everlastingsummer/";
    
    public static String getSdCardPath() {
        return Environment.getExternalStorageDirectory().getPath() + "/"; 
    }
    
    public static Bitmap readBitmap(String file) throws IOException {
        final InputStream is = open(file);
        final Bitmap result = BitmapFactory.decodeStream(is);
        is.close();
        return result;
    }
    
    public static List<SaveInfo> listSaves(Context context) {
        final String[] fileslist = context.fileList();
        final List<SaveInfo> saves = new ArrayList<SaveInfo>(fileslist.length);
        for (String filename : fileslist) {
            try {
                saves.add(readSaveInfo(context, filename));
            } catch (IOException ioe) {
                if (Logger.DEBUG) Logger.log("listSaves: " + filename, ioe);
            }
        }
        return saves;
    }
    
    public static SaveInfo readSaveInfo(Context context, String filename) throws IOException {
        final InputStream is = context.openFileInput(filename);
        final DataInputStream dis = new DataInputStream(is);
        final SaveInfo result = SaveInfo.readFromStream(dis);
        dis.close();
        return result;
    }
    
    public static void writeSaveInfo(Context context, String filename, SaveInfo saveInfo) throws IOException {
        final OutputStream os = context.openFileOutput(filename, Context.MODE_PRIVATE);
        final DataOutputStream dos = new DataOutputStream(os);
        saveInfo.writeToStream(dos);
        dos.flush();
        dos.close();
    }
    
    public static void removeSaveInfo(Context context, String filename) {
        final File file = context.getFileStreamPath(filename);
        if (file != null) {
            file.delete();
        }
    }
    
    public static AssetFileDescriptor getAFD(String file) throws IOException {
        return ViewActivity.getInstance().getAssets().openFd(ASSETS + file);
    }
    
    private static FileInputStream streamForFD;
    public static FileDescriptor getFD(String file) throws IOException {
        if (streamForFD != null) streamForFD.close();
        streamForFD = new FileInputStream(ASSETS + file);
        return streamForFD.getFD();
    }
    
    public static InputStream open(String file) throws IOException {
        if (useArchive) return ViewActivity.getInstance().getAssets().open(ASSETS + file);
        return new FileInputStream(ASSETS + file);
    }

    public static String readContents(InputStream is) throws IOException {
        final StringBuilder sb = new StringBuilder();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while ( (line = reader.readLine()) != null ) {
            sb.append(line);
            sb.append("\n");
        }
        reader.close();
        return sb.toString();
    }
}
