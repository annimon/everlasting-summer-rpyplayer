﻿scene bg int_musclub_day
"Я зашёл в музыкальный клуб."
label start:
show mi shocked pioneer at center
$ renpy.say(mi, "Меня Мику зовут.", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Нет, честно-честно!", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Никто не верит, а меня правда так зовут.", interact=False)
$ renpy.pause(0.25, hard=True)
show mi normal pioneer at center
$ renpy.say(mi, "Просто у меня мама из Японии.", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Папа с ней познакомился, когда строил там…", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Ну, то есть не строил – он у меня инженер…", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Короче, атомную станцию!", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Или плотину…", interact=False)
$ renpy.pause(0.25, hard=True)
$ renpy.say(mi, "Или мост…", interact=False)
$ renpy.pause(0.25, hard=True)
show mi happy pioneer at center
$ renpy.say(mi, "Ну, неважно!", interact=False)
$ renpy.pause(0.25, hard=True)
me "Это что сейчас было???"
me "Повтори, пожалуйста!"
jump start