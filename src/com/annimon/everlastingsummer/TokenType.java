package com.annimon.everlastingsummer;

/**
 * @author aNNiMON
 */
public enum TokenType {
    
    COMMAND, // начинается с $
    WORD,
    TEXT,
    NUMBER,
    
    // операторы и спецсимволы
    EQ,
    PLUS,
    MINUS,
    LT,
    GT,
    LPAREN,
    RPAREN,
    LBRACKET,
    RBRACKET,
    EXCL,
    COLON,
    
    // ключевые слова
    PLAY,
    QUEUE,
    STOP,
    MUSIC,
    AMBIENCE,
    SOUND,
    SOUNDLOOP,
    FADEIN,
    FADEOUT,
    
    SCENE,
    ANIM,
    BG,
    CG,
    
    WINDOW,
    HIDE,
    SHOW,
    
    AT,
    AS,
    WITH,
    DEFINE,
    
    MENU,
    ENDMENU,
    JUMP,
    LABEL,
    RETURN,
    
    IF,
    ELSE,
    ENDIF,
    OR,
    AND,
    NOT,
    
    // команды
    RENPY_PAUSE,
    RENPY_SAY,
    PERSISTENT_SPRITE_TIME,
    PROLOG_TIME,
    DAY_TIME,
    SUNSET_TIME,
    NIGHT_TIME,
    MAKE_NAMES_KNOWN,
    MAKE_NAMES_UNKNOWN,
    SET_NAME,
    DISABLE_ALL_ZONES,
    DISABLE_CURRENT_ZONE,
    RESET_ZONE,
    SET_ZONE,
    SHOW_MAP,
    
    EOF
}
