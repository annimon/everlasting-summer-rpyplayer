package com.annimon.everlastingsummer;

import java.util.HashMap;
import java.util.Map;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Места на карте.
 * @author aNNiMON
 */
public class MapPlaces {
    
    /** Маппинг <игровое названия, полное название> */
    protected final Map<String, String> names;
    
    protected Map<String, String> zones;
    protected String currentZone;
    
    public MapPlaces() {
        names = new HashMap<String, String>();
        zones = new HashMap<String, String>();
        currentZone = "";
    }

    public void disableAllZones() {
        zones.clear();
        currentZone = "";
    }
    
    public void disableCurrentZone() {
        zones.remove(currentZone);
    }
    
    public void resetZone(String zone) {
        zones.remove(zone);
    }
    
    public void setZone(String name, String label) {
        zones.put(name, label);
    }
    
    public void showMap(Context context) {
        final int size = zones.size();
        final String[] zoneKeys = new String[size];
        final String[] zoneNames = new String[size];
        int i = 0;
        for (Map.Entry<String, String> zone : zones.entrySet()) {
            final String key = zone.getKey();
            zoneKeys[i] = key;
            zoneNames[i] = names.containsKey(key) ? names.get(key) : key;
            i++;
        }
        Dialogs.with(context).showMapMenu(zoneNames, new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentZone = zoneKeys[which];
                Parser.getInstance().jumpLabel(zones.get(currentZone));
                Parser.getInstance().next();
            }
        });
    }
}
