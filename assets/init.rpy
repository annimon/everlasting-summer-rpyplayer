# Сценарий для автозапуска
#rp.scenario = "meet_you_there.rpy"

# Путь к ресурсам
# sdcard - предустановленный путь к карте памяти, напр. /mnt/sdcard/
# archive - предустановленный путь к архиву программы (папка assets)
#rp.assets = archive + "everlastingsummer/"
rp.assets = sdcard + "everlastingsummer/"

# Прочие настройки
# анимация переходов для спрайтов
rp.sprite_transitions = false

# Использовать стартовое меню
rp.menu = false
rp.menu_background = "bg/ext_road_night.jpg"
rp.menu_items = 4

rp.menu_item1.text = "Начать"
rp.menu_item1.x = 960
rp.menu_item1.y = 560
rp.menu_item1.font = 25
rp.menu_item1.color = "#FFFFFFFF"
rp.menu_item1.action = "meet_you_there.rpy"

rp.menu_item2.text = "Загрузить"
rp.menu_item2.x = 960
rp.menu_item2.y = 720
rp.menu_item2.font = 25
rp.menu_item2.color = white
rp.menu_item2.action = load

rp.menu_item3.text = "Выход"
rp.menu_item3.x = 960
rp.menu_item3.y = 880
rp.menu_item3.font = 25
rp.menu_item3.color = white
rp.menu_item3.action = exit

# используется для вывода текста (action не указан)
rp.menu_item4.text = "v" + version
rp.menu_item4.x = 60
rp.menu_item4.y = 1000
rp.menu_item4.font = 16
rp.menu_item4.color = "#7AFFFFFF" # прозрачность 7A