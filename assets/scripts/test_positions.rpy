$ day_time()
$ persistent.sprite_time = "day"
play music music_list["everlasting_summer"]
scene bg ext_square_day
show mz smile pioneer far at fright
show us grin sport far at fleft
show mt smile pioneer far at right
show mi smile pioneer far at left
show dv smile pioneer2  at cright
show sl smile pioneer  at cleft
show un smile2 pioneer  at center
all "Приглашаем тебя в Бесконечное Лето!"

window hide
$ renpy.pause(2)
window show

mt "Ладненько, я побежала, а ты пока можешь осмотреть лагерь!{w} Вечером приходи на ужин, не забудь!"
hide mt
"Ушла"
show cs smile far at right
cs "И ко мне загляни… пионер."