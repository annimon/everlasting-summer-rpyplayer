package com.annimon.everlastingsummer.ast;

/**
 * @author aNNiMON
 */
public interface Expression {
    
    double eval();
}
