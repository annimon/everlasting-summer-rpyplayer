package com.annimon.everlastingsummer.ast;

/**
 * @author aNNiMON
 */
public class ValueExpression implements Expression {

    private final double value;
    
    public ValueExpression(double value) {
        this.value = value;
    }

    @Override
    public double eval() {
        return value;
    }
}
