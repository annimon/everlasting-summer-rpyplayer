package com.annimon.everlastingsummer;

import android.util.Log;

/**
 * Класс логгирования.
 * @author aNNiMON
 */
public final class Logger {

    public static final boolean DEBUG = false;
    private static final String TAG = "rpyplayer";
    
    public static void log(String message) {
        Log.d(TAG, message);
    }
    
    public static void log(Exception ex) {
        log("", ex);
    }
    
    public static void log(String message, Exception ex) {
        Log.e(TAG, message, ex);
    }
}
