﻿play music music_list["gentle_predator"]
$ meet_all = 0
$ date_lena = False

label start:
scene black
if meet_all:
  $ make_names_known()
  "Мы всех знаем"
else:
  $ make_names_unknown()
  "Мы никого не знаем"
endif

label meet_od:
scene bg ext_square_day
show mt smile pioneer at center
mt "Привет, Семён. Меня зовут Ольга Дмитриевна, я твоя вожатая."
if meet_all: th "Я знаю, женщина! В прошлый раз ты уже представлялась."
else: th "Да она не старше меня! Буду звать её просто, Ольга."
endif
$ meet('mt', 'Ольга')
show mt grin pioneer at center
mt "В обед за сахаром сбегай!"
hide mt
if meet_all: me "Я ведь уже бегал!"
else: th "Странно, едва познакомились, а она уже напрягать меня задумала. Вот стерва!"
endif

show un shy pioneer at center
me "Привет."
show un shocked pioneer at center
un "Ой, привет."
if meet_all:
  me "В прошлый раз ты так быстро убежала, а я хотел немного побыть в твоей компании."
  show un shy pioneer at center
  "Лена покраснела и уставилась в пол."
  un "Прости..."
  me "Нет-нет, ты что, я не требую извинений! Напротив, это я должен извиниться перед тобой."
  menu:
    "Предложить встретиться":
        $ date_lena = True
        me "Давай прогуляемся вечером?"
        show un grin pioneer at center
        un "Давай."
    "Не предлагать":
        $ date_lena = False
        $ renpy.pause(2)
        "Я снова стоял, не в силах что-либо произнести."
  endmenu
else:
  me "Как тебя зовут, о, прелестное создание?"
  show un shy pioneer at center
  un "Л-лена."
  $ meet('un', 'Лена')
  $ renpy.pause(0.7)
  un "А тебя?"
  me "Семён."
  show un grin pioneer at center
  un "Очень приятно."
endif
un "Ну ладно, мне надо бежать."
show un grin pioneer far at center
$ meet('you_and_me', u'Семён и Лена')
you_and_me "Увидимся!"
"Одновременно прокричали мы с ней."
hide un
th "Замечательная девушка!"

$ renpy.pause(1.6)

scene bg ext_polyana_day
"Я вышел на поляну."
play sound sfx_bush_leaves
"Внезапно послышался шорох."
show uv normal far
"И я увидел девочку."
"И я увидел девочку. С кошачьими ушками."
show uv shocked at center
me "Прости, я не хотел тебя напугать."
show uv surprise at center
me "Как тебя зовут?"
show uv surprise2 at center
uv "Зовут?"
me "Ну, имя у тебя есть? Меня вот Семёном величать."
show uv upset at center
uv "Не знаю..."
menu: "Как назвать девочку?"
  "Юля": $ meet('uv', 'Юля') me "Тогда буду звать тебя Юлей."
  "Катя": $ meet('uv', 'Катя') me "Тогда буду звать тебя Катей."
  "Мурка":
      $ meet('uv', 'Мурка')
      me "Тогда буду звать тебя Муркой."
      show uv shocked at center
      uv "Да ты с ума сошёл!"
      uv "Отправляйся на ещё один виток!"
      jump meet_od
  "Вика": $ meet('uv', 'Вика') me "Тогда буду звать тебя Викой."
endmenu
show uv laugh at center
uv "Хорошее имя."
$ renpy.pause(2)
"Я стоял, не в силах что-либо произнести."
$ renpy.pause(2)
if not meet_all:
  uv "А ты мне сахар можешь принести?"
  th "Да что ж творится-то? Всем нужен сахар!"
  me "Принесу."
  show uv grin at center
  uv "Вот и хорошо! Жду тебя вечером здесь."
else:
  show uv grin at center
  uv "Кстати, спасибо за сахар!"
endif
play sound sfx_bush_leaves
hide uv
"С этими словами она скрылась за кустами."

$ meet_all = not meet_all
# первый раз переходим в начало, а потом уже в конец
if meet_all:
  jump start
endif

scene black
"Конец"
$ renpy.pause(1)
"А хотя..."
$ renpy.pause(1)
if date_lena:
  scene bg ext_square_night
  play music music_list["confession_oboe"]
  $ meet('me', 'Ромео')
  $ meet('un', 'Джульетта')
  
  me "Святая ночь, святая ночь! А вдруг
Все это сон, так непомерно счастье
Так сказочно и чудно это все!"
  
  show un sad dress at center
  un "Ромео, где ты? Дудочку бы мне,
Чтоб эту птичку приманить обратно!"
  un "Но я в неволе, мне кричать нельзя,
А то б я эхо довела до хрипа
Немолчным повтореньем этих слов:"
  show un cry dress at center
  un "Ромео, где ты? Где же ты, Ромео?"
  me "Моя душа зовет меня опять.
Как звонки ночью голоса влюбленных!"
  show un cry_smile dress at center
  un "Ромео!"
  me "Милая!"
  un "В каком часу
Послать мне завтра за ответом?"
  me "В девять."
  un "До этого ведь целых двадцать лет!
Я изождусь… Что я сказать хотела?"
  me "Припомни, я покамест постою."
  un "Постой, покамест я опять забуду,
Чтоб только удержать тебя опять."
  me "Припоминай и забывай, покуда,
Себя не помня, буду я стоять."
  show un normal dress at center
  un "Почти рассвет. Пора тебе исчезнуть.
А как, скажи, расстаться мне с тобой?"
  un "Ты, как ручная птичка щеголихи,
Прикованная ниткою к руке."
  un "Ей то дают взлететь на весь подвесок,
То тащат вниз на шелковом шнурке.
Вот так и мы с тобой."
  me "Я был бы счастлив
Быть этой птицей."
  show un grin dress at center
  un "Рада бы и я,
Да я б тебя убила частой лаской."
  show un cry_smile dress at center
  un "Однако окончательно прощай."
  un 'Прощай, прощай, а разойтись нет мочи!
Так и твердить бы век: «Спокойной ночи!».'
  hide un
  me "Прощай. Спокойный сон к тебе приди
И сладкий мир разлей в твоей груди!"
else:
  scene cg d4_el_wash
  play music music_list["awakening_power"]
  $ meet('ksa', 'Ксакеп')
  ksa "Shut up and sleep with me!"
endif
$ renpy.pause(5)