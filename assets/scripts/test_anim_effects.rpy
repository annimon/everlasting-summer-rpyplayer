﻿"{center}{html}<h1>Тест анимационных эффектов</h1>"

scene bg int_bus with flash
"В глаза ударил яркий дневной свет."
scene bg ext_bus
with dissolve
scene anim intro_14
with fade2
play sound sfx_intro_bus_transition
$ renpy.pause(3, hard=True)
scene anim intro_15
with fade
$ renpy.pause(3, hard=True)
scene anim intro_16
with fade


window hide
$ renpy.pause(3, hard=True)


scene bg ext_aidpost_night
with dissolve
play ambience ambience_camp_center_night fadein 3
window show
"Остальную часть пути мы шли молча."
"Лена, очевидно, слишком смутилась от моего неумелого комплимента, а я просто не знал, что сказать, какую тему придумать для разговора."
"Уже полностью стемнело, и мрачное здание медпункта, окутанное ночным туманом, больше походило на дом с привидениями."
"Мне вдруг захотелось развернуться и медленно, не создавая лишнего шума, уйти отсюда."
show un normal dress at center
with dissolve
"Я украдкой бросил взгляд на Лену и отметил, что она выглядит как обычно – застенчивой, скромной, неуверенной в себе, но никак не испуганной."
"Тем больше мне стало не по себе."
th "Да как это так – она не боится, а я..."
play sound sfx_owl_far
"Внезапно где-то рядом послышалось уханье совы, от чего меня буквально передёрнуло."
"А Лена либо не слышала, либо не обратила внимания, либо ей и не было страшно вовсе."
stop ambience fadeout 2
"В третий вариант верилось с трудом, но и спрашивать, чтобы не выдать свой испуг, не хотелось."
window hide
scene bg black
with dissolve
play music "goodbye_home_shores" fadein 3
window show
"Войдя в медпункт, я нащупал в темноте выключатель и включил свет."
window hide
scene bg int_aidpost_night
with flash
play ambience ambience_medstation_inside_night fadein 3
show un normal dress at center 
with dissolve
window show
me "А медсестра позже придёт, да?"
un "А она не придёт…"
stop music

window hide
$ renpy.pause(2, hard=True)


scene bg ext_path_night with fade
"Перед глазами пронеслась красная вспышка."
scene bg black with flash_red
play sound sfx_alisa_falls
"Раздался крик Двачевской и звук падающего тела."
sl "Алиса!"
"Положение становилось всё хуже и хуже."
play sound sfx_ignite_torch
un "Сейчас будет свет!"
"С её рук слетели огоньки и подожгли наши факелы."
scene bg ext_path_night with fade
"На небе снова появилась луна."
"С земли уже поднималась Алиса." 
show dv angry pioneer close with dissolve
dv "Ох, больно то как."
show sl serious pioneer close at left with dissolve 
sl "С тобой всё в порядке?"
dv "В полном. {w}Жду не дождусь, когда смогу поквитаться с тем, кто это сделал."
"Выглядела она неплохо, учитывая, что в неё попало что-то магическое и явно опасное." 
me "Идти сможешь?"
show dv grin pioneer close with dspr
dv "Да хоть бегать!" 
"К счастью, она действительно оказалась в порядке. Повезло так повезло."
show un normal pioneer close at right with dissolve
un "Кто бы на нас ни напал, его уже нет. Давайте двинемся дальше."
hide dv with dissolve
hide un with dissolve
hide sl with dissolve


window hide
$ renpy.pause(2, hard=True)


scene black with dissolve
play sound sfx_signal_pistol
"Я достал пистолет из-за пояса, направил его в сторону наугад и выстрелил."
window hide
scene bg int_mine_room
with flash_red
show un normal pioneer red at center 
with dissolve
window show
"Комната тут же озарилась ярким красным светом."


window hide
$ renpy.pause(2, hard=True)


"Откуда-то начинает бить нестерпимо яркий свет, и я вынужден закрыть глаза."
scene white with flash
"Почему-то начал дуть ветер."
"Откуда ветер в пещере?"
scene bg ext_bathhouse_night with dspr
"Я оказался у дома из начала нашего приключения."