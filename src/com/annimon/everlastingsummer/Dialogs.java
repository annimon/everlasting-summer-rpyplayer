package com.annimon.everlastingsummer;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Работа с диалогами.
 * @author aNNiMON
 */
public final class Dialogs {
    
    public static Dialogs with(Context context) {
        return new Dialogs(context);
    }
    
    private final Context context;
    private final AlertDialog.Builder builder;
    
    private Dialogs(Context context) {
        this.context = context;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.Dialog));
    }

    public void showMenu(int itemsId, DialogInterface.OnClickListener listener) {
        builder.setTitle(R.string.actions);
        builder.setItems(itemsId, listener);
        builder.setNegativeButton(android.R.string.cancel, dismissDialog);
        builder.setCancelable(true);
        builder.show();
    }
    
    public void showGameMenu(Menu menu, DialogInterface.OnClickListener listener) {
        if (menu.getTitle() == null) builder.setTitle(R.string.choose);
        else builder.setTitle(menu.getTitle());
        builder.setItems(menu.getItemsNames(), listener);
        builder.setCancelable(false);
        builder.show();
    }
    
    public void showMapMenu(String[] zoneNames, DialogInterface.OnClickListener listener) {
        builder.setTitle(R.string.map);
        builder.setItems(zoneNames, listener);
        builder.setCancelable(false);
        builder.show();
    }

    @SuppressLint("InflateParams")
    public void showNavigate() {
        final View root = LayoutInflater.from(context).inflate(R.layout.navigate, null);
        final TextView positionIndicator = (TextView) root.findViewById(R.id.position);
        final SeekBar slider = (SeekBar) root.findViewById(R.id.slider);
        final TextView text = (TextView) root.findViewById(R.id.text);
        
        final Parser parser = Parser.getInstance();
        positionIndicator.setText(String.valueOf(parser.getPosition()));
        slider.setMax(parser.getTokensCount() - 1);
        slider.setProgress(parser.getPosition());
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
            
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }
            
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                positionIndicator.setText(String.valueOf(progress));
                if (progress >= parser.getTokensCount()) return;
                text.setText(parser.getTokens().get(progress).getText());
            }
        } );
        
        builder.setTitle(R.string.navigate);
        builder.setView(root);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                parser.setPosition(slider.getProgress());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, dismissDialog);
        builder.setCancelable(false);
        builder.show();
    }
    
    public void showSaves(final List<SaveInfo> saves, DialogInterface.OnClickListener listener) {
        builder.setTitle(R.string.load);
        builder.setAdapter(new SavesAdapter(context, saves), listener);
        builder.setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Dialogs.with(context).showSavesRemove(saves);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, dismissDialog);
        builder.setCancelable(true);
        builder.show();
    }
    
    public void showSavesRemove(final List<SaveInfo> saves) {
        final int size = saves.size();
        final String[] items = new String[size];
        final DateFormat format = DateFormat.getDateTimeInstance(
                DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault());
        for (int i = 0; i < size; i++) {
            final SaveInfo info = saves.get(i);
            items[i] = format.format(info.getTime()) + "\n" + info.getPath();
        }
        final boolean[] checkedItems = new boolean[size];

        builder.setTitle(R.string.remove);
        builder.setMultiChoiceItems(items, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                checkedItems[which] = isChecked;
            }
        });
        
        builder.setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean removed = false;
                for (int i = 0; i < size; i++) {
                    if (checkedItems[i]) {
                        IOUtil.removeSaveInfo(context, Long.toString(saves.get(i).getTime()));
                        removed = true;
                    }
                }
                if (removed) {
                    Toast.makeText(context, R.string.removed, Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, dismissDialog);
        builder.setCancelable(true);
        builder.show();
    }

    private final DialogInterface.OnClickListener dismissDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };
}
