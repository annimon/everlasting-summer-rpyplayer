# Список анимационных переходов.
# Поддерживаются Dissolve (http://www.renpy.org/doc/html/transitions.html#Dissolve) и Fade (http://www.renpy.org/doc/html/transitions.html#Fade) переходы

# Fade to black and back.
define fade = Fade(0.5, 0.0, 0.5)
define fade2 = Fade(1, 0.0, 1)
define fade3 = Fade(1.5, 0.0, 1.5)

# Hold at black for a bit.
define fadehold = Fade(0.5, 1.0, 0.5)

# Camera flash - quickly fades to white, then back to the scene.
define flash = Fade(0.1, 0.0, 0.5, color="#fff")
define flash2 = Fade(2, 2, 2, color="#fff")
define flash_red = Fade(1, 0, 1, color="#ffee1111")

define dspr = Dissolve(0.2)
define dissolve = Dissolve(1)
define dissolve2 = Dissolve(2)