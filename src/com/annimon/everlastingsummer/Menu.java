package com.annimon.everlastingsummer;

import java.util.ArrayList;
import java.util.List;

/**
 * Меню внутриигрового выбора.
 * @author aNNiMON
 */
public final class Menu {

    private final String title;
    private final List<Item> items;
    
    public Menu(String title) {
        this.title = title;
        items = new ArrayList<Item>();
    }
    
    public String getTitle() {
        return title;
    }

    public void addItem(String name, int position) {
        items.add(new Item(name, position));
    }
    
    public String[] getItemsNames() {
        final int size = items.size();
        final String[] result = new String[size];
        for (int i = 0; i < size; i++) {
            result[i] = items.get(i).name;
        }
        return result;
    }
    
    public int getPosition(int itemIndex) {
        return items.get(itemIndex).position;
    }

    private final class Item {
        final String name;
        final int position;

        Item(String name, int position) {
            this.name = name;
            this.position = position;
        }
    }
}
