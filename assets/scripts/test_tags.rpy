﻿scene bg black
"Основные теги:\n{b}жирный{/b} {i}курсив{/i} {u}подчёркнутый{/u}"
"{big}большой{/big} нормальный {small}маленький{/small}"
"{center}текст по центру"
sl "{center}текст по центру"
"{html}{center}<h1>HTML</h1>"
"{html}<big>Большой big</big>"
"{html}<small>Маленький small</small>"
"{html}<b>Жирный bold</b> <strong>strong</strong>"
"{html}<i>Курсив i</i> <em>em</em> <cite>cite</cite> <dfn>dfn</dfn>"
"{html}<u>Подчёркнутый</u>"
"{html}<blockquote><p>Длинная цитата</p>blockquote</blockquote>"
"{html}<br/><br/>Переносы<br/><br/>строк<br/><br/><br/><br/>br<br/>"
"{html}<tt>Моноширинный tt</tt>"
"{html}<sub>Подстрочный sub</sub> Нормальный <sup>Надстрочный sup</sup>"
'{html}<a href="http://annimon.com/">Ссылка a href</a>'
"{html}<h1>Заголовок h1</h1>
<h2>Заголовок h2</h2>
<h3>Заголовок h3</h3>
<h4>Заголовок h4</h4>
<h5>Заголовок h5</h5>
<h6>Заголовок h6</h6>"
'{html}<font color="red">font color="red"</font>
       <font color="purple">font color="purple"</font>
       <font color="#336699">font color="#336699"</font>
       <font color="#ffffff">font color="#ffffff"</font>'
'{html}<font face="serif">font face="serif"</font>
       <font face="sans-serif">font face="sans-serif"</font>
       <font face="monospace">font face="monospace"</font>'
'{html}<font face="monospace" color="#7f7f7f">font face="monospace" color="#7f7f7f"</font>'
"{html}<p>Параграф</p><p>p</p>"
"{html}<div>Блок div</div>"