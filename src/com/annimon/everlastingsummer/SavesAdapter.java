package com.annimon.everlastingsummer;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author aNNiMON
 */
public class SavesAdapter extends BaseAdapter {
    
    private final LayoutInflater mLayoutInflater;
    private List<SaveInfo> mList; 

    public SavesAdapter(Context context, List<SaveInfo> objects) {
        mLayoutInflater = LayoutInflater.from(context);
        mList = objects;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public SaveInfo getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(android.R.layout.simple_list_item_2, parent, false);
            holder = new ViewHolder();
            holder.time = (TextView) convertView.findViewById(android.R.id.text1);
            holder.path = (TextView) convertView.findViewById(android.R.id.text2);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        final SaveInfo save = mList.get(position);
        if (save != null) {
            final DateFormat format = DateFormat.getDateTimeInstance(
                    DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault());
            holder.time.setText(format.format( new Date(save.getTime()) ));
            holder.path.setText(save.getPath());
        }
        return convertView;
    }

    private static final class ViewHolder {
        TextView time, path;
    }
}
