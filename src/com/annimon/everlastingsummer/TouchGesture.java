package com.annimon.everlastingsummer;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Распознавание жестов.
 * @author aNNiMON
 */
public final class TouchGesture implements View.OnTouchListener, GestureDetector.OnGestureListener {

    public enum TouchGestureType {
        SINGLE_TAP,
        SWIPE
    }

    public static interface OnTouchGestureListener {
        void onTouchGesture(TouchGestureType type);
    }

    private static final int SWIPE_MIN_DISTANCE = 100;
    private static final int SWIPE_MIN_VELOCITY = 100;

    private final GestureDetector detector;
    private final OnTouchGestureListener listener;

    public TouchGesture(Context context, OnTouchGestureListener listener) {
        detector = new GestureDetector(context, TouchGesture.this);
        this.listener = listener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        detector.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        listener.onTouchGesture(TouchGestureType.SINGLE_TAP);
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        final float distanceX = Math.abs(e1.getX() - e2.getX());
        final float distanceY = Math.abs(e1.getY() - e2.getY());
        final boolean isNormalDistance = (distanceX > SWIPE_MIN_DISTANCE || distanceY > SWIPE_MIN_DISTANCE);
        final boolean isNormalVelocity = (velocityX > SWIPE_MIN_VELOCITY || velocityY > SWIPE_MIN_VELOCITY);
        if (isNormalDistance && isNormalVelocity) {
            listener.onTouchGesture(TouchGestureType.SWIPE);
            return true;
        }
        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) { }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) { }
}
