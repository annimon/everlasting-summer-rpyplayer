package com.annimon.everlastingsummer.transitions;

import java.util.HashMap;
import java.util.Map;
import android.graphics.Color;

/**
 * Хранилище эффектов переходов.
 * @author aNNiMON
 */
public final class Transitions {
    
    private static final Map<String, Transition> transitions;
    static {
        transitions = new HashMap<String, Transition>();
        transitions.put("fade", new Fade(500, 0, 500));
        transitions.put("fade2", new Fade(1000, 0, 1000));
        transitions.put("fade3", new Fade(1500, 0, 1500));
        transitions.put("dspr", new Dissolve(200));
        transitions.put("dissolve", new Dissolve(1000));
        transitions.put("dissolve2", new Dissolve(2000));
        transitions.put("flash", new Fade(1000, 0, 1000, Color.WHITE));
        transitions.put("flash2", new Fade(2000, 2000, 2000, Color.WHITE));
        transitions.put("flash_red", new Fade(1000, 0, 1000, 0xFFEE1111));
    }
    
    public static void set(String name, Transition transition) {
        transitions.put(name, transition);
    }

    public static boolean contains(String name) {
        return transitions.containsKey(name);
    }

    public static Transition get(String name) {
        return transitions.get(name);
    }
}
