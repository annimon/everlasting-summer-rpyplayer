package com.annimon.everlastingsummer;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import com.annimon.everlastingsummer.transitions.TransitionsParser;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.*;

/**
 * Экран выбора сценариев из папки assets.
 * @author aNNiMON
 */
@SuppressWarnings("deprecation")
public final class MainActivity extends Activity {
    
    private static final double VIRTUAL_WIDTH = 1920d, VIRTUAL_HEIGHT = 1080d;
    private static final String ARCHIVE = "archive://";
    private static final String LOAD = "%load%";
    private static final String EXIT = "%exit%";
    
    private String[] scripts;
    private boolean needAlignment;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseConfigs();
    }
    
    private void parseConfigs() {
        try {
            parseTransitions();
        } catch (IOException ioe) {
            if (Logger.DEBUG) Logger.log("parseTransitions", ioe);
        }
        try {
            parseInitConfig();
        } catch (IOException ioe) {
            if (Logger.DEBUG) Logger.log("parseInitConfig", ioe);
            scriptListMode();
        }
    }
    
    private void parseTransitions() throws IOException {
        final InputStream is = getAssets().open("transitions.rpy");
        TransitionsParser.parse(Lexer.tokenize( IOUtil.readContents(is) ));
    }
    
    private void parseInitConfig() throws IOException {
        final InputStream is = getAssets().open("init.rpy");
        final ConfigParser config = ConfigParser.parse(Lexer.tokenize( IOUtil.readContents(is) ));
        config.addValue("sdcard", IOUtil.getSdCardPath());
        config.addValue("archive", ARCHIVE);
        try {
            config.addValue("version", getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (NameNotFoundException ex) {
            if (Logger.DEBUG) Logger.log("version", ex);
        }
        config.addValue("load", LOAD);
        config.addValue("exit", EXIT);
        config.parse();
        
        // Настройка пути к ресурсам
        if (config.isValueExists("rp.assets")) {
            final String path = config.getValue("rp.assets");
            if (path.startsWith(ARCHIVE)) {
                // Возможность работы с ресурсами внутри программы
                IOUtil.useArchive = true;
                IOUtil.ASSETS  = path.substring(ARCHIVE.length());
            } else {
                IOUtil.useArchive = false;
                IOUtil.ASSETS = path;
            }
        }
        // Автозапуск скрипта
        if (config.isValueExists("rp.scenario")) {
            finish();
            openScenario(config.getValue("rp.scenario"));
        }
        // Эффекты переходов для спрайтов
        if (config.isValueExists("rp.sprite_transitions")) {
            ViewActivity.useSpriteTransitions = config.getValueAsBoolean("rp.sprite_transitions");
        }
        // Меню
        if (config.getValueAsBoolean("rp.menu")) {
            menuMode(config);
        } else {
            scriptListMode();
        }
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        
        if (!needAlignment) return;
        
        needAlignment = false;
        
        final RelativeLayout root = (RelativeLayout) findViewById(R.id.root);
        if (root == null) return;
        
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View v = root.getChildAt(i);
            if (v instanceof Button) {
                final Button button = (Button) v;
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) button.getLayoutParams();
                params.leftMargin -= button.getWidth() / 2;
                params.topMargin -= button.getHeight() / 2;
            }
        }
    }
    
    private void openScenario(String name) {
        final Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra(ViewActivity.EXTRA_NAME, name);
        startActivity(intent);
    }
    
    
    /*
     * Выбор скриптов из списка в папке assets/scripts 
     */
    private void scriptListMode() {
        needAlignment = false;
        final ListView list = new ListView(this);
        try {
            scripts = getAssets().list(PathResolver.SCRIPT_ASSETS);
        } catch (IOException ioe) {
            scripts = null;
        }
        if (scripts == null || scripts.length == 0) {
            Toast.makeText(this, getString(R.string.no_scripts, PathResolver.SCRIPT_ASSETS),
                    Toast.LENGTH_LONG).show();
            finish();
        }
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, scripts));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openScenario(scripts[position]);
            }
        });
        setContentView(list);
    }
    
    /*
     * Парсинг и показ меню с пунктами в конфиге init.rpy
     */
    private void menuMode(ConfigParser config) {
        needAlignment = true;
        // Полноэкранный режим
        setTheme(R.style.FullscreenTheme);
        // Размеры экрана для правильного позиционирования элементов
        final DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        final int width = displaymetrics.widthPixels;
        final int height = displaymetrics.heightPixels;
        
        setContentView(R.layout.launcher);
        if (config.isValueExists("rp.menu_background")) {
            final ImageView background = (ImageView) findViewById(R.id.background);
            try {
                // IOUtil обращается к ViewActivity при useArchive, что приведёт к NPE
                // поэтому загружаем вручную
                if (IOUtil.useArchive) {
                    background.setImageDrawable(new BitmapDrawable( getResources(),
                            getAssets().open(IOUtil.ASSETS + config.getValue("rp.menu_background"))
                            ));
                } else {
                    background.setImageBitmap(IOUtil.readBitmap(config.getValue("rp.menu_background")));
                }
            } catch (IOException ioe) {
                Logger.log("menu background", ioe);
            }
        }
        
        final RelativeLayout root = (RelativeLayout) findViewById(R.id.root);
        final int items = (int) config.getValueAsDouble("rp.menu_items");
        for (int i = 0; i < items; i++) {
            parseMenuItem(config, root, width, height, i+1);
        }
    }
    
    private void parseMenuItem(ConfigParser config, RelativeLayout root, int width, int height, int index) {
        final String key = "rp.menu_item" + index + ".";
        
        final Button button = new Button(this);
        button.setBackgroundDrawable(null);
        
        // текст
        if (config.isValueExists(key + "text")) {
            button.setText(config.getValue(key + "text"));
        }
        // шрифт
        if (config.isValueExists(key + "font")) {
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (float)config.getValueAsDouble(key + "font"));
        }
        // цвет текста
        if (config.isValueExists(key + "color")) {
            button.setTextColor(parseColor(config.getValue(key + "color")));
        }
        
        // позиция
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        if (config.isValueExists(key + "x")) {
            final double x = config.getValueAsDouble(key + "x");
            params.leftMargin = (int)(x * width / VIRTUAL_WIDTH);
        }
        if (config.isValueExists(key + "y")) {
            final double y = config.getValueAsDouble(key + "y");
            params.topMargin = (int)(y * height / VIRTUAL_HEIGHT);
        }
        
        // действие
        if (config.isValueExists(key + "action")) {
            final String action = config.getValue(key + "action");
            if (LOAD.equalsIgnoreCase(action)) {
                button.setOnClickListener(loadListener);
            } else if (EXIT.equalsIgnoreCase(action)) {
                button.setOnClickListener(exitListener);
            } else {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openScenario(action);
                    }
                });
            }
        }
        
        root.addView(button, params);
    }

    private int parseColor(String value) {
        try {
            return Color.parseColor(value);
        } catch (IllegalArgumentException iae) {
            // #fff
            int index = value.charAt(0) == '#' ? 1 : 0;
            if (value.length() != (index + 3)) return Color.BLACK;
            final StringBuilder sb = new StringBuilder(7);
            sb.append("#");
            sb.append(value.charAt(index)).append(value.charAt(index));
            index++;
            sb.append(value.charAt(index)).append(value.charAt(index));
            index++;
            sb.append(value.charAt(index)).append(value.charAt(index));
            return parseColor(sb.toString());
        }
    }
    
    private void showLoadStateDialog() {
        final List<SaveInfo> saves = IOUtil.listSaves(getApplicationContext());
        if (saves == null || saves.isEmpty()) {
            Toast.makeText(this, R.string.no_saves, Toast.LENGTH_SHORT).show();
            return;
        }
        Dialogs.with(this).showSaves(saves, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final SaveInfo save = saves.get(which);
                // Пересоздаём активити
                final Intent intent = new Intent(MainActivity.this, ViewActivity.class);
                intent.putExtra(ViewActivity.EXTRA_SAVE, save);
                startActivity(intent);
            }
        });
    }
    
    private final View.OnClickListener loadListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showLoadStateDialog();
        }
    };
    
    private final View.OnClickListener exitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
