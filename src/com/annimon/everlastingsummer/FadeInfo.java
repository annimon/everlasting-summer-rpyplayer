package com.annimon.everlastingsummer;

/**
 * Информация о плавном переходе состояния воспроизведения.
 * @author aNNiMON
 */
public final class FadeInfo {

    private boolean in, out;
    private double duration;
    
    public FadeInfo() {
        this(false, false, 0);
    }
    
    public FadeInfo(boolean in, boolean out, double duration) {
        this.in = in;
        this.out = out;
        this.duration = duration;
    }

    public boolean isIn() {
        return in;
    }
    
    public void setIn(boolean in) {
        this.in = in;
    }
    
    public boolean isOut() {
        return out;
    }
    
    public void setOut(boolean out) {
        this.out = out;
    }
    
    public double getDuration() {
        return duration;
    }
    
    public void setDuration(double duration) {
        this.duration = duration;
    }
}
