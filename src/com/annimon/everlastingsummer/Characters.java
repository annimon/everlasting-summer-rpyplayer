package com.annimon.everlastingsummer;

import java.util.HashMap;
import java.util.Map;

/**
 * Работа с персонажами.
 * @author aNNiMON
 */
public class Characters {

    private static final int DEFAULT_COLOR = 0xFFC0C0C0;
    
    /** Маппинг <короткое имя, полное имя/цвет> */
    protected final Map<String, NameInfo> names;
    
    public Characters() {
        this.names = new HashMap<String, NameInfo>();
        makeNamesUnknown();
    }
    
    public boolean contains(String shortName) {
        return names.containsKey(shortName);
    }
    
    public NameInfo get(String shortName) {
        return names.get(shortName);
    }
    
    public void setName(String shortName, String fullName) {
        if (names.containsKey(shortName)) {
            NameInfo info = names.get(shortName);
            info.name = fullName;
        } else {
            names.put(shortName, new NameInfo(fullName));
        }
    }
    
    public void setName(String shortName, String fullName, int color) {
        if (names.containsKey(shortName)) {
            NameInfo info = names.get(shortName);
            info.name = fullName;
            info.color = color;
        } else {
            names.put(shortName, new NameInfo(fullName, color));
        }
    }
    
    public void makeNamesUnknown() {
        names.clear();
    }
    
    public void makeNamesKnown() {
    }

    public static class NameInfo {
        String name;
        int color;
        protected NameInfo(String fullName) {
            this.name = fullName;
            this.color = DEFAULT_COLOR;
        }
        protected NameInfo(String fullName, int color) {
            this.name = fullName;
            this.color = color;
        }
    }
}
