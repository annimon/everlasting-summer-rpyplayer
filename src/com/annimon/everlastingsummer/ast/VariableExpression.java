package com.annimon.everlastingsummer.ast;

/**
 * @author aNNiMON
 */
public class VariableExpression implements Expression {

    private final String variable;
    
    public VariableExpression(String variable) {
        this.variable = variable;
    }

    @Override
    public double eval() {
        return Variables.getVariable(variable);
    }
}
