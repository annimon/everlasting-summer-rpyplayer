package com.annimon.everlastingsummer.transitions;

import android.graphics.Color;

/**
 * Плавный эффект перехода: img1 -> color -> img2
 * @author aNNiMON
 */
public class Fade implements Transition {
    
    private final int outTime;
    private final int holdTime;
    private final int inTime;
    private final int color;
    
    public Fade(int outTime, int holdTime, int inTime) {
        this(outTime, holdTime, inTime, Color.BLACK);
    }
    
    public Fade(int outTime, int holdTime, int inTime, int color) {
        this.outTime = outTime;
        this.holdTime = holdTime;
        this.inTime = inTime;
        this.color = color;
    }
    
    @Override
    public int type() {
        return TYPE_FADE;
    }
    
    public int getOutTime() {
        return outTime;
    }
    
    public int getHoldTime() {
        return holdTime;
    }

    public int getInTime() {
        return inTime;
    }
    
    public int getColor() {
        return color;
    }

}
