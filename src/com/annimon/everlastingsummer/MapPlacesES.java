package com.annimon.everlastingsummer;

/**
 * Места на карте для бесконечного лета.
 * @author aNNiMON
 */
public final class MapPlacesES extends MapPlaces {

    public MapPlacesES() {
        names.put("admin_house","Админ. корпус");
        names.put("badminton","Бадминтон");
        names.put("beach","Пляж");
        names.put("boat_station","Лодочная станция");
        names.put("camp_entrance", "Ворота в лагерь");
        names.put("clubs", "Клубы");
        names.put("dining_hall", "Столовая");
        names.put("dv_us_house", "Домик Ульяны и Алисы");
        names.put("estrade", "Сцена");
        names.put("forest", "Лес");
        names.put("island_nearest", "Остров \"Ближний\"");
        names.put("library", "Библиотека");
        names.put("me_mt_house", "Домик вожатой");
        names.put("medic_house", "Медпункт");
        names.put("monument", "Памятник");
        names.put("music_club", "Музыкальный клуб");
        names.put("old_house", "Старый корпус");
        names.put("old_road", "Дорога к старому лагерю");
        names.put("sl_mz_house", "Домик Слави и Жени");
        names.put("sport_area", "Спортплощадка");
        names.put("square", "Площадь");
        names.put("store_house", "Склад");
        names.put("sy_sh_house", "Домик Электроника и Шурика");
        names.put("un_mi_house", "Домик Лены и Мику");
        names.put("valleyball", "Волейбол");
        names.put("wash_house", "Душевая");
    }
}
