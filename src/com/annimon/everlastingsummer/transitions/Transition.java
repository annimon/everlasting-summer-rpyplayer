package com.annimon.everlastingsummer.transitions;

/**
 * Эффект перехода.
 * @author aNNiMON
 */
public interface Transition {

    public static final int TYPE_FADE = 0;
    public static final int TYPE_DISSOLVE = 1;
    
    int type();
}
