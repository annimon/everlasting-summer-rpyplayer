package com.annimon.everlastingsummer.transitions;

/**
 * Плавное растворение img1 -> img2
 * @author aNNiMON
 */
public class Dissolve implements Transition {
    
    private final int delay;
    
    public Dissolve(int delay) {
        this.delay = delay;
    }
    
    @Override
    public int type() {
        return TYPE_DISSOLVE;
    }
    
    public int getDelay() {
        return delay;
    }
}
